package me.bumblebeee_.glowGrenade;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class GlowGrenade extends JavaPlugin {
	
	public void onEnable() {
		Bukkit.getServer().getPluginManager().registerEvents(new InteractEvent(this), this);
		saveDefaultConfig();
	}

}
