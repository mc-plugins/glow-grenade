package me.bumblebeee_.glowGrenade;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class InteractEvent implements Listener {
	
	Plugin pl = null;
	public InteractEvent(Plugin plugin) {
		pl = plugin;
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if (e.getPlayer().getInventory().contains(Material.SULPHUR)) {
			if (e.getItem() != null) {
				ItemStack i = e.getItem();
				if (i.getType() == Material.SULPHUR) {
					ItemStack drop = new ItemStack(Material.SULPHUR, 1);
					e.getPlayer().getInventory().removeItem(drop);
					Item dropped = e.getPlayer().getWorld().dropItemNaturally(e.getPlayer().getEyeLocation(), drop);
					dropped.setVelocity(e.getPlayer().getEyeLocation().getDirection().multiply(0.6));
					trackItem(dropped);
				}
			}
		}
	}
	
	public void trackItem(final Item d) {
		new BukkitRunnable(){
            @SuppressWarnings("deprecation")
			@Override
            public void run() {
            	final int r = pl.getConfig().getInt("effect-radius");
            	final int duration = pl.getConfig().getInt("duration");
            	if (d.getVelocity().getY() == 0) {
            		cancel();
            		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(pl, new BukkitRunnable() {
            			public void run() {
            				for (Entity e : d.getNearbyEntities(r, r, r)) {
                    			if (e instanceof LivingEntity) {
                    				((LivingEntity) e).addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, duration * 20, 0));
                    			}
                    		}

                    		d.remove();
            			}
            		}, 40);
                }
            }
		}.runTaskTimer(pl, 0, 10);
	}

}
